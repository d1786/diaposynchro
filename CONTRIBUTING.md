# Contributing

([Français](#contribution))

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a 
   build.
2. Update the README.md with details of changes to the interface, this includes new environment 
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. 
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you 
   do not have permission to do that, you may request the second reviewer to merge it for you.

   If this is your first time contributing on GitHub, don't worry! [Let us know via email if you have any questions](mailto:lehoang.math@gmail.com).

______________________

# Contribution

Lorsque vous contribuez à ce répertoire, veuillez d’abord discuter du changement que vous souhaitez apporter par l’entremise du problème.
e-mail, ou toute autre méthode avec les propriétaires de ce dépôt avant de faire un changement. 

Veuillez noter que nous avons un code de conduite, veuillez le suivre dans toutes vos interactions avec le projet.

## Processus de demande de pull

1. S’assurer que toutes les dépendances d’installation ou de construction sont supprimées avant la fin de la couche 
   construire.
2. Mettre à jour le site README.md avec les détails des changements apportés à l’interface, ce qui comprend le nouvel environnement. 
   variables, ports exposés, emplacements de fichiers utiles et paramètres de conteneur.
3. Augmentez les numéros de version dans les fichiers d’exemples et le fichier README.md dans la nouvelle version.
   Pull Request représenterait. 
4. Vous pouvez fusionner la demande d’extraction une fois que vous avez obtenu l’approbation de deux autres développeurs, ou si vous 
   n’ont pas la permission de le faire, vous pouvez demander au deuxième examinateur de le fusionner pour vous.

   Si c’est votre première contribution sur GitHub, ne vous inquiétez pas! [Faites-nous savoir par courriel si vous avez des questions](mailto:lehoang.math@gmail.com).
