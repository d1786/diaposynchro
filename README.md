<div align="center">
  <img src="https://firebasestorage.googleapis.com/v0/b/diaposynchro-c7cdb.appspot.com/o/Frame%202.png?alt=media&token=8b0c3e13-7a65-43f5-87d1-de323129be5b" />
</div>

# DiapoSynchro

DiapoSynchro is a tool that allows speakers to syncronize a diaporama and a video presentation for a better replay experience for their audiences.

The website takes only PowerPoint presentations recordings for now. It will also automatically generate a summary of keypoints (linked with timecodes) from the presentation. The backend is handle via Firebase which is managed by * [@chaigqn](https://gitlab.com/chaigqn).

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have read the [documentation](https://gitlab.com/d1786/diaposynchro/-/wikis/home)
* Create a new Firebase project and create a web application.

## Installing DiapoSynchro

To install DiapoSynchro, follow these steps:

```
git clone https://gitlab.com/d1786/diaposynchro.git
cd diaposynchro
npm install
```

## Using DiapoSynchro

To use DiapoSynchro, follow these steps:
Copy the firebase config in the environment variables file.

```
npm run dev
```

## Contributing to DiapoSynchro

First please read the [code of conduct](https://gitlab.com/d1786/diaposynchro/-/blob/main/CODE%20OF%20CONDUCT.md).

If you find a bug during the use of the API, do not hesitate to open an issue here.

For new functionnalities proposal, please do not hesitate to directly contact us by mail (see contact section). 

To add a new functionnality follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name_describing_the_added_functionalities>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin diaposynchro/develop`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).


## Contributors ✨

Thanks to the following people who have contributed to this project:

* [@SenseiOrsay](https://gitlab.com/SenseiOrsay) 🖥
* [@Maximat](https://gitlab.com/Maximat)

## Contact

If you want to contact me you can reach me at quentinchaignauduniv@gmail.com.

## License

This project uses the following license: [Apache License 2.0](https://gitlab.com/d1786/diaposynchro/-/blob/main/LICENSE).
