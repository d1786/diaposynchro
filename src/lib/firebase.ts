import { initializeApp } from "firebase/app"

const firebaseConfig = {
    apiKey: "AIzaSyD0uwuhZqsYAMqGiocOzIQ0MoR09KvFrwI",
    authDomain: "diaposynchro-c7cdb.firebaseapp.com",
    projectId: "diaposynchro-c7cdb",
    storageBucket: "diaposynchro-c7cdb.appspot.com",
    messagingSenderId: "1044826759011",
    appId: "1:1044826759011:web:b56a7e8f9bc1e8ae713d56"
};

export const app = initializeApp(firebaseConfig)