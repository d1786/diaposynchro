module.exports = {
    mode: 'jit',
    purge: ['./src/**/*.svelte'],
    darkMode: 'class',
    theme: {
        extend: {
            boxShadow: {
                DEFAULT: '0px 8px 24px rgba(149, 157, 165, 0.2)',
                deep: '0px 7px 29px 0px rgba(149, 157, 165, 0.2)'
            },
            width: {
                'diapo': '50rem',
            },
            colors: {
                gray: {
                    light: '#778DBD',
                    medium: '#CCCCCC',
                    DEFAULT: '#35393F',
                    dark: '#475B87',
                },
                blue: {
                    DEFAULT: '#7496F9',
                    dark: '#2B313D',
                },
                green: {
                    DEFAULT: '#30C48C'
                },
                red: {
                    DEFAULT: '#FC5B48'
                },
            },
            borderWidth: {
                '3': '3px',
            },
            boxShadow: {
                DEFAULT: '0px 8px 24px rgba(149, 157, 165, 0.2)',
                deep: '0px 7px 29px 0px rgba(149, 157, 165, 0.2)'
            },
            fontSize: {
                'xxs': '.5rem',
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}